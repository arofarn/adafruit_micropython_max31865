"""Simple demo of the MAX31865 thermocouple amplifier.
Will print the temperature every second."""
import time
import machine
import adafruit_max31865


# Initialize SPI bus and sensor.
SPI = machine.SPI(-1, sck=machine.Pin(14, machine.Pin.OUT),
                  mosi=machine.Pin(13, machine.Pin.OUT),
                  miso=machine.Pin(12, machine.Pin.OUT)
                  )
SPI.init(baudrate=115200, polarity=0, phase=1)
CS = machine.Pin(2)
SENSOR = adafruit_max31865.MAX31865(SPI, CS)
# Note you can optionally provide the thermocouple RTD nominal, the reference
# resistance, and the number of wires for the sensor (2 the default, 3, or 4)
# with keyword args:
# sensor = adafruit_max31865.MAX31865(spi, cs, rtd_nominal=100, ref_resistor=430.0, wires=2)

# Main loop to print the temperature every second.
while True:
    # Read temperature.
    TEMP = SENSOR.temperature
    # Print the value.
    print("Temperature: {0:0.3f}C".format(TEMP))
    # Delay for a second.
    time.sleep(1.0)
