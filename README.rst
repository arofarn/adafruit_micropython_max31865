
Introduction
============

.. image:: https://readthedocs.org/projects/adafruit-circuitpython-max31865/badge/?version=latest
    :target: https://circuitpython.readthedocs.io/projects/max31865/en/latest/
    :alt: Documentation Status

.. image :: https://img.shields.io/discord/327254708534116352.svg
    :target: https://discord.gg/nBQh6qu
    :alt: Discord

Micropython port of the CircuitPython module for the MAX31865 thermocouple amplifier.

Dependencies
=============
This driver depends on:

* `MicroPython <https://github.com/micropython/micropython>`_

Usage Example
=============

See examples/max31865_simpletest.py for a demo of the usage.

Contributing
============

Contributions are welcome! Please read our `Code of Conduct
<https://github.com/adafruit/Adafruit_CircuitPython_max31865/blob/master/CODE_OF_CONDUCT.md>`_
before contributing to help this project stay welcoming.

Documentation
=============

For information on building library documentation, please check out `this guide <https://learn.adafruit.com/creating-and-sharing-a-circuitpython-library/sharing-our-docs-on-readthedocs#sphinx-5-1>`_.
